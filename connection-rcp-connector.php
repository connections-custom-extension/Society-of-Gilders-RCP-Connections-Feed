<?php
/**
 * @package   Connections Business Directory Connector - Restrict Content Pro SOG Feed
 * @category  Extension
 * @author    Steven A. Zahm
 * @license   GPL-2.0+
 * @link      https://connections-pro.com
 * @copyright 2022 Steven A. Zahm
 *
 * @wordpress-plugin
 * Plugin Name:       Connections Business Directory Connector - Restrict Content Pro SOG Feed
 * Plugin URI:        https://connections-pro.com/
 * Description:       Creates a Connections Directory Entry on Restrict Content Pro registration and updates Connections Entry with changes made on the Restrict Content Pro Profile page.
 * Version:           2.0
 * Author:            Steven A. Zahm
 * Author URI:        https://connections-pro.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       connections-rcp-feed-connector
 * Domain Path:       /languages
 */

namespace Connections_Directory\Connector;

use cnEntry_Action;
use cnGeo;
use cnTerm;
use cnTerm_Object;
use WP_Error;
use WP_User;

if ( ! class_exists( 'Restrict_Content_Pro_SOG_Feed' ) ) {

	final class Restrict_Content_Pro_SOG_Feed {

		const VERSION = '1.0.0';

		/**
		 * Stores the instance of this class.
		 *
		 * @since 2.0
		 * @var static
		 */
		private static $instance;

		/**
		 * @var string The absolute path this file.
		 *
		 * @since 2.0
		 */
		private $file = '';

		/**
		 * @var string The URL to the plugin's folder.
		 *
		 * @since 2.0
		 */
		private $url = '';

		/**
		 * @var string The absolute path to this plugin's folder.
		 *
		 * @since 2.0
		 */
		private $path = '';

		/**
		 * @var string The basename of the plugin.
		 *
		 * @since 2.0
		 */
		private $basename = '';

		/**
		 * @since 2.0
		 * @var array[]
		 */
		private $fields = array(
			'BusName' => array(
				'label'                => 'Business Name',
				'type'                 => 'text',
				'show_in_admin_header' => true,
				'description'          => 'The member\'s business name',
				'error'                => false
			),
			'Phone1'  => array(
				'label'                => 'Cell Phone',
				'type'                 => 'text',
				'show_in_admin_header' => true,
				'description'          => 'The member\'s cell phone',
				'error'                => 'Please enter your cell phone'
			),
			'Phone2'  => array(
				'label'                => 'Other Phone',
				'type'                 => 'text',
				'show_in_admin_header' => true,
				'description'          => 'The member\'s other phone',
				'error'                => false
			),
			'Address1'  => array(
				'label'                => 'Street Address',
				'type'                 => 'text',
				'show_in_admin_header' => true,
				'description'          => 'The member\'s street address',
				'error'                => 'Please enter your street address'
			),
			'Address2'  => array(
				'label'                => 'Additional Address Lines',
				'type'                 => 'text',
				'show_in_admin_header' => true,
				'description'          => 'The member\'s street address 2',
				'error'                => false
			),
			'City'      => array(
				'label'                => 'City',
				'type'                 => 'text',
				'show_in_admin_header' => true,
				'description'          => 'The member\'s city or town',
				'error'                => 'Please enter your city'
			),
			'State'     => array(
				'label'                => 'State',
				'type'                 => 'text',
				'show_in_admin_header' => true,
				'description'          => 'The member\'s state or province',
				'error'                => 'Please enter your state or province'
			),
			'Zip'       => array(
				'label'                => 'Zip',
				'type'                 => 'text',
				'show_in_admin_header' => true,
				'description'          => 'The member\'s zip or postal code',
				'error'                => 'Please enter your zip or postal code'
			),
			'Country'   => array(
				'label'                => 'Country',
				'type'                 => 'text',
				'show_in_admin_header' => true,
				'description'          => 'The member\'s country',
				'error'                => 'Please enter your country'
			),
			'Interests' => array(
				'label'                => 'Interests',
				'type'                 => 'checkbox',
				'show_in_admin_header' => false,
				'values'               => array(
					'Appraiser',
					'Architectural Gilding',
					'Ceramicist',
					'Conservator',
					'Decorative Artist',
					'Designer',
					'Exterior Gilding',
					'Frames',
					'Freelance Gilder',
					'Furniture',
					'Gallery',
					'Glass Gilder',
					'Gilding Supplies',
					'Iconography',
					'Instructor',
					'Leather Gilding',
					'Lecturer',
					'Manufacturer',
					'Manuscript Illumination',
					'Museum',
					'Restorer',
					'Retail',
					'School',
					'Sculptor',
					'Signage',
					'Studio',
					'Wholesale',
					'Wood Carver'
				),
				'description'          => 'The member\'s interests',
				'error'                => false,
			),
		);

		/**
		 * A dummy constructor to prevent Restrict_Content_Pro_SOG_Feed from being loaded more than once.
		 *
		 * @since 2.0
		 */
		private function __construct() { /* Do nothing here */ }

		final public static function instance() {

			if ( ! isset( self::$instance ) && ! ( self::$instance instanceof self ) ) {

				$self = new self();

				$self->file     = __FILE__;
				$self->url      = plugin_dir_url( $self->file );
				$self->path     = plugin_dir_path( $self->file );
				$self->basename = plugin_basename( $self->file );

				$self->add_admin_table_custom_field_metadata();
				$self->hooks();

				do_action( 'Connections_Directory/RCP_SOG_Connector/Init', $self );

				self::$instance = $self;
			}

			return self::$instance;
		}

		/**
		 * Register hooks.
		 *
		 * @internal
		 * @since 2.0
		 */
		private function hooks() {

			add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_script' ) );
			add_action( 'wp_footer', array( $this, 'enqueue_script' ) );

			add_action( 'rcp_after_password_registration_field', array( $this, 'registration_fields' ) );
			add_action( 'rcp_profile_editor_after', array( $this, 'registration_fields' ) );
			add_action( 'rcp_edit_member_after', array( $this, 'rcp_edit_member_after' ) );

			add_action( 'rcp_form_errors', array( $this, 'rcp_form_errors' ), 10, 2 );
			add_action( 'rcp_form_processing', array( $this, 'rcp_form_processing' ), 10, 2 );

			add_action( 'rcp_user_profile_updated', array( $this, 'rcp_save_fields' ), 10 );
			add_action( 'rcp_edit_member', array( $this, 'rcp_save_fields' ), 10 );

			add_filter( 'rcp_export_csv_cols_members', array( $this, 'rcp_export_csv_cols_members' ) );
			add_filter( 'rcp_export_memberships_get_data_row', array( $this, 'rcp_export_memberships_get_data_row' ), 10, 2 );

			add_action( 'rcp_csv_import_membership_processed', array( $this, 'rcp_csv_import_membership_processed' ), 10, 3 );
			add_filter( 'rcp_csv_importers', array( $this, 'rcp_csv_importers' ) );

			add_filter( 'rcp_memberships_list_table_columns', array( $this, 'rcp_memberships_list_table_columns' ) );

			add_filter( 'Connections_Directory/RCP_SOG_Connector/Field/Metadata', array( $this, 'populate_state_field_metadata' ), 10, 3 );
			add_filter( 'Connections_Directory/RCP_SOG_Connector/Field/Metadata', array( $this, 'populate_country_field_metadata' ), 10, 3 );
			add_action( 'Connections_Directory/Taxonomy/Init', array( $this, 'populate_specialty_field_metadata' ) );

			// Set priority 11 to ensure other actions hooked on priority 10 which must run first.
			add_action( 'rcp_form_processing', array( $this, 'create_entry' ), 11, 2 );
			add_action( 'rcp_user_profile_updated', array( $this, 'update_entry' ), 11, 1 );
			add_action( 'rcp_edit_member', array( $this, 'update_entry' ), 11, 1 );

			// Back compatibility filter to normalize the country field values.
			add_filter(
				'Connections_Directory/RCP_SOG_Connector/Field/Value',
				function( $value, $field_key ) {

					if ( 'Country' !== $field_key ) {

						return $value;
					}

					$lower = strtolower( $value );

					if ( 'italia' === $lower ) {

						$value = 'Italy';

					// Scotland is a country, but, not an independent country as it is still part of the United Kingdom.
					} elseif ( 'scotland' === $lower ) {

						$value = 'United Kingdom';

					} elseif ( 'u.k.' === $lower || 'u.k' === $lower || 'uk' === $lower ) {

						$value = "United Kingdom";

					} elseif ( 'us' === $lower || 'usa' === $lower ) {

						$value = "United States";
					}

					return $value;
				},
				10,
				2
			);

			// Back compatibility filter to normalize the US State field values.
			add_filter(
				'Connections_Directory/RCP_SOG_Connector/Field/Value',
				function( $value, $field_key ) {

					if ( 'State' !== $field_key ) {

						return $value;
					}

					$upper   = strtoupper( $value );
					$regions = cnGeo::getRegions( 'US' );

					if ( array_key_exists( $upper, $regions ) ) {

						return $regions[ $upper ];
					}

					return $value;
				},
				10,
				2
			);
		}

		/**
		 * Return the regions based on the users country.
		 *
		 * @since 2.0
		 *
		 * @param false|int $user_id WP User ID.
		 *
		 * @return array
		 */
		private function get_regions_for_user( $user_id ) {

			$regions = cnGeo::getRegions( 'US' );

			if ( is_numeric( $user_id ) ) {

				$value = $this->get_field_data( 'Country', $user_id );

				// Add filter so the back compatibility filter can alter the value as needed tp return the correct country regions.
				$value = apply_filters(
					'Connections_Directory/RCP_SOG_Connector/Field/Value',
					$value,
					'Country',
					array(),
					$user_id
				);

				$countries = cnGeo::getCountries();

				foreach ( $countries as $code => $country ) {

					if ( $value === $country ) {

						return cnGeo::getRegions( $code );
					}
				}
			}

			return $regions;
		}

		/**
		 * Callback for the `Connections_Directory/RCP_SOG_Connector/Field/Metadata` filter.
		 * Populate field metadata for the region (state/province) field.
		 *
		 * @internal
		 * @since 2.0
		 */
		public function populate_state_field_metadata( $field, $field_key, $user_id ) {

			if ( 'State' !== $field_key ) {

				return $field;
			}

			$regions = $this->get_regions_for_user( $user_id );

			$label = 'State';
			$field = array(
				'label'                => $label,
				'type'                 => 'select',
				'show_in_admin_header' => true,
				'values'               => array(),
				'description'          => 'The member\'s state or province.',
				'error'                => 'Please enter your state or province.',
			);

			foreach ( $regions as $code => $name ) {

				$field['values'][ $code ] = $name;
			}

			return $field;
		}

		/**
		 * Callback for the `Connections_Directory/RCP_SOG_Connector/Field/Metadata` filter.
		 * Populate field metadata for the country field.
		 *
		 * @internal
		 * @since 2.0
		 */
		public function populate_country_field_metadata( $field, $field_key, $user_id ) {

			if ( 'Country' !== $field_key ) {

				return $field;
			}

			$countries = cnGeo::getCountries();

			$label = 'Country';
			$field = array(
				'label'                => $label,
				'type'                 => 'select',
				'show_in_admin_header' => true,
				'values'               => array(),
				'description'          => 'The member\'s country.',
				'error'                => 'Please enter your country.',
			);

			foreach ( $countries as $code => $name ) {

				$field['values'][ $code ] = $name;
			}

			return $field;
		}

		/**
		 * Callback for the `Connections_Directory/Taxonomy/Init` action.
		 * Dynamically populate the "Specialties" options from the "Specialty" Connections Category children.
		 *
		 * @internal
		 * @since 2.0
		 */
		public function populate_specialty_field_metadata() {

			$terms = cnTerm::getTaxonomyTerms(
				'category',
				array(
					// 'fields'     => 'id=>name',
					'child_of'   => 33,
					'exclude'    => 31, // The "Uncategorized" term.
					'hide_empty' => false,
				)
			);

			if ( ! empty( $terms ) ) {

				$name = 'Interests';
				$meta = array(
					'label'                => 'Specialties',
					'type'                 => 'checkbox',
					'show_in_admin_header' => false,
					'values'               => array(),
					'description'          => 'The member\'s specialties.',
					'error'                => false,
				);

				foreach ( $terms as $term ) {

					if ( is_object( $term ) ) {

						$meta['values'][] = $term->name;
					}
				}

				$this->fields[ $name ] = $meta;

				$this->fields['Suggest Specialty'] = array(
					'label'                => 'Suggest a Specialty',
					'type'                 => 'text',
					'show_in_admin_header' => false,
					'description'          => 'Member suggested specialty.',
					'error'                => false,
				);
			}
		}

		/**
		 * Display the custom RCP field metadata oin the admin table utilizing an anonymous callback.
		 *
		 * @since 2.0
		 */
		private function add_admin_table_custom_field_metadata() {

			foreach ( $this->fields as $field => $data ) {

				if ( true === $data['show_in_admin_header'] ) {

					add_filter(
						'rcp_memberships_list_table_column_' . $this->sanitize_key( $field ),
						/**
						 * @param string          $value      Column value.
						 * @param \RCP_Membership $membership Membership object.
						 *
						 * @return array|string
						 */
						function( $value, $membership ) use ( $field, $data ) {

							$meta = $this->get_field_data( $field, $membership->get_customer()->get_user_id() );

							if ( is_array( $meta ) ) {

								return $meta;

							} else {

								return esc_html( $meta );
							}

						},
						10,
						2
					);
				}
			}
		}

		/**
		 * Sanitize and prefix field key.
		 *
		 * @param string $field The field key {@see Restrict_Content_Pro_SOG_Feed::$fields}.
		 *
		 * @return string
		 */
		private function sanitize_key( $field ) {

			return '_sog_' . sanitize_title( $field );
		}

		/**
		 * Get metadata.
		 *
		 * @since 2.0
		 *
		 * @param string    $field
		 * @param int|false $user_id Current WP User ID.
		 *
		 * @return array|string
		 */
		private function get_field_data( $field, $user_id = false ) {

			if ( false === $user_id ) {
				$user_id = get_current_user_id();
			}

			$meta = get_user_meta( $user_id, $this->sanitize_key( $field ), true );

			if ( is_array( $meta ) ) {

				return $meta;

			} else {

				return trim( $meta );
			}
		}

		/**
		 * Callback for the `rcp_after_password_registration_field` and `rcp_profile_editor_after` actions.
		 *
		 * @internal
		 * @since 2.0
		 *
		 * @param int|false $user_id Current WP User ID.
		 */
		public function registration_fields( $user_id = false ) {

			foreach ( $this->fields as $key => $field ) {

				$field = apply_filters(
					'Connections_Directory/RCP_SOG_Connector/Field/Metadata',
					$field,
					$key,
					$user_id
				);

				$id   = $this->sanitize_key( $key );
				$type = $field['type'];

				?>
				<p id="<?php echo esc_attr( "{$id}_wrap" ); ?>">
					<?php
					switch ( $type ) {

						case 'checkbox':
						case 'select':
							?>
							<span style="display: block"><?php echo esc_html( $field['label'] ); ?></span>
							<?php
							break;

						case 'text':
							?>
							<label for="<?php echo esc_attr( $id ); ?>"><?php echo esc_html( $field['label'] ); ?></label>
							<?php
							break;
					}
					?>
					<?php $this->field( $key, $field ); ?>
				</p>
				<?php
			}
		}

		/**
		 * Callback for the `rcp_edit_member_after` action.
		 *
		 * @internal
		 * @since 2.0
		 *
		 * @param int|false $user_id Current WP User ID.
		 */
		public function rcp_edit_member_after( $user_id = false ) {

			foreach ( $this->fields as $key => $field ) {

				$field = apply_filters(
					'Connections_Directory/RCP_SOG_Connector/Field/Metadata',
					$field,
					$key,
					$user_id
				);

				$type = $field['type'];

				?>
				<tr>
					<th scope="row">
						<label for="<?php echo $this->sanitize_key( $key ); ?>"><?php echo esc_html( $field['label'] ); ?></label>
					</th>
					<td>
						<?php
						switch ( $type ) {

							case 'checkbox':
							case 'select':
								?>
								<p class="description"><?php echo esc_html( $field['description'] ); ?></p>
								<?php
								$this->field( $key, $field, $user_id );
								break;

							case 'text':
								$this->field( $key, $field, $user_id );
								?>
								<p class="description"><?php echo esc_html( $field['description'] ); ?></p>
								<?php
								break;
						}
						?>
					</td>
				</tr>
				<?php
			}
		}

		/**
		 * Render the custom RCP field.
		 *
		 * @internal
		 * @since 2.0
		 *
		 * @param string    $field_key The field key.
		 * @param array     $field     The field metadata.
		 * @param int|false $user_id   Current WP User ID.
		 */
		private function field( $field_key, $field, $user_id = false ) {

			$name  = $this->sanitize_key( $field_key );
			$type  = $field['type'];
			$value = $this->get_field_data( $field_key, $user_id );

			$value = apply_filters(
				'Connections_Directory/RCP_SOG_Connector/Field/Value',
				$value,
				$field_key,
				$field,
				$user_id
			);

			switch ( $type ) {

				case 'checkbox':

					echo '<span class="checkbox-columns" style="display: grid; grid-auto-columns: max-content; grid-template-columns: repeat( auto-fit, minmax( 200px, 1fr ) );">';

					foreach ( $field['values'] as $key => $option ) {

						$id      = $this->sanitize_key( $option );
						$checked = '';

						if ( is_array( $value ) ) {

							if ( in_array( $option, $value ) ) {

								$checked = 'checked="checked"';
							}

						} elseif ( strtolower( $value ) === strtolower( $option ) ) {

							$checked = 'checked="checked"';
						}

						?>
						<span style="align-items: center; display: flex; margin: 0;">
							<input name="<?php echo esc_attr( $name ); ?>[]" id="<?php echo esc_attr( $id ); ?>" type="checkbox" value="<?php echo esc_attr( $option ); ?>" <?php echo $checked; ?> style="float: none; margin: 0 6px 0 0;"/>
							<label for="<?php echo esc_attr( $id ); ?>"><?php echo esc_html( $option ); ?></label>
						</span>
						<?php
					}

					echo '</span>';

					break;

				case 'select':

					$id = $this->sanitize_key( $field_key );

					?>
					<select name="<?php echo esc_attr( $name ); ?>" id="<?php echo esc_attr( $id ); ?>">
						<?php

						foreach ( $field['values'] as $key => $option ) {

							$id       = $this->sanitize_key( $option );
							$selected = '';

							if ( is_array( $value ) ) {

								if ( in_array( $option, $value ) ) {

									$selected = 'selected="selected"';
								}

							} elseif ( strtolower( $value ) === strtolower( $option ) ) {

								$selected = 'selected="selected"';
							}
							?>
							<option id="<?php echo esc_attr( $id ); ?>" value="<?php echo esc_attr( $option ); ?>" <?php echo $selected ?>><?php echo esc_html( $option ); ?></option>
							<?php
						}
						?>
					</select>
					<?php

					break;

				case 'text':
					$id = $this->sanitize_key( $field_key );
					?>
					<input name="<?php echo esc_attr( $name ); ?>" id="<?php echo esc_attr( $id ); ?>" type="text" value="<?php echo esc_attr( $value ); ?>"/>
					<?php
					break;
			}
		}

		/**
		 * Callback for the `rcp_form_errors` action.
		 * Add RCP field validation error.
		 *
		 * @internal
		 * @since 2.0
		 *
		 * @param array               $posted
		 * @param \RCP_Customer|false $customer
		 */
		public function rcp_form_errors( $posted, $customer ) {

			if ( is_user_logged_in() ) {
				return;
			}

			foreach ( $this->fields as $field => $data ) {

				if ( false !== $data['error'] ) {

					if ( empty( $posted[ $this->sanitize_key( $field ) ] ) ) {
						rcp_errors()->add( 'invalid_' . $this->sanitize_key( $field ), $data['error'], 'register' );
					}
				}
			}
		}

		/**
		 * Callback for the `rcp_form_processing` action.
		 * Update the form registration.
		 *
		 * @internal
		 * @since 2.0
		 *
		 * @param array $posted  Copy of the `$_POST` global.
		 * @param int   $user_id Current WP User ID.
		 */
		public function rcp_form_processing( $posted, $user_id ) {

			foreach ( $this->fields as $field => $data ) {

				if ( ! empty( $posted[ $this->sanitize_key( $field ) ] ) ) {

					update_user_meta( $user_id, $this->sanitize_key( $field ), $posted[ $this->sanitize_key( $field ) ] );
				}
			}
		}

		/**
		 * Callback for the `rcp_user_profile_updated` and `rcp_edit_member` actions.
		 * Update the form profile fields.
		 *
		 * @internal
		 * @since 2.0
		 *
		 * @param int $user_id Current WP User ID.
		 */
		public function rcp_save_fields( $user_id ) {

			foreach ( $this->fields as $field => $data ) {

				if ( ! empty( $_POST[ $this->sanitize_key( $field ) ] ) ) {

					update_user_meta( $user_id, $this->sanitize_key( $field ), $_POST[ $this->sanitize_key( $field ) ] );
				}
			}
		}

		/**
		 * Callback for the `rcp_export_csv_cols_members` filter.
		 * Add CSV column header.
		 *
		 * @internal
		 * @since 2.0
		 *
		 * @param array $columns
		 *
		 * @return array
		 */
		public function rcp_export_csv_cols_members( $columns ) {

			foreach ( $this->fields as $field => $data ) {

				$columns[ $this->sanitize_key( $field ) ] = $data['label'];
			}

			return $columns;
		}

		/**
		 * Callback for the `rcp_export_memberships_get_data_row` filter.
		 * Add data to CSV export.
		 *
		 * @internal
		 * @since 2.0
		 *
		 * @param array           $row        Membership data for this row.
		 * @param \RCP_Membership $membership Membership object.
		 *
		 * @return array
		 */
		public function rcp_export_memberships_get_data_row( $row, $membership ) {

			foreach ( $this->fields as $field => $data ) {

				$row[ $this->sanitize_key( $field ) ] = $this->get_field_data( $field, $membership->get_customer()->get_user_id() );
			}

			return $row;
		}

		/**
		 * Callback for the `rcp_csv_import_membership_processed` action.
		 * Import and/or update member accounts from a CSV file.
		 *
		 * @internal
		 * @since 2.0
		 *
		 * @param \RCP_Membership $membership The membership that was imported or updated.
		 * @param WP_User         $user       User object associated with the new membership.
		 * @param array           $row        Entire CSV row for this membership.
		 *
		 * @return void
		 */
		public function rcp_csv_import_membership_processed( $membership, $user, $row ) {

			foreach ( $this->fields as $field => $data ) {

				if ( ! empty( $row[ $field ] ) ) {

					if ( 'text' === $data['type'] ) {

						$update_value = $row[ $field ];

					} elseif ( 'checkbox' === $data['type'] ) {

						$update_value = explode( ',', $row[ $field ] );
					}

					if ( isset( $update_value ) ) {

						update_user_meta( $user->ID, $this->sanitize_key( $field ), $update_value );
					}
				}
			}
		}

		/**
		 * Callback for the `rcp_csv_importers` filter.
		 * Add the fields to the import csv form.
		 *
		 * @internal
		 * @since 2.0
		 *
		 * @param array $importers
		 *
		 * @return array
		 */
		public function rcp_csv_importers( $importers ) {

			foreach ( $this->fields as $field => $data ) {

				$importers['memberships']['columns'][ $this->sanitize_key( $field ) ] = $data['label'];
			}

			return $importers;
		}

		/**
		 * Callback for the `rcp_memberships_list_table_columns` filter.
		 * Add columns to admin table.
		 *
		 * @internal
		 * @since 2.0
		 *
		 * @param array $columns
		 *
		 * @return array
		 */
		public function rcp_memberships_list_table_columns( $columns ) {

			foreach ( $this->fields as $field => $data ) {

				if ( true === $data['show_in_admin_header'] ) {

					$columns[ $this->sanitize_key( $field ) ] = $data['label'];
				}
			}

			return $columns;
		}

		/**
		 * Return array of countries and their regions.
		 *
		 * @since 2.0
		 *
		 * @return array{name: string, regions: string[]}
		 */
		public function get_country_region_choices() {

			$choices   = array();
			$countries = cnGeo::getCountries();

			foreach ( $countries as $code => $country ) {

				$regions          = cnGeo::getRegions( $code );
				$choices[ $code ] = array(
					'name'    => $country,
					'regions' => $regions,
				);
			}

			return $choices;
		}

		/**
		 * Callback for the `admin_enqueue_scripts` and `wp_footer` actions.
		 *
		 * @since 2.0
		 */
		public function enqueue_script() {

			// This variable is set to TRUE if one of the RCP shortcodes are used on a page/post.
			global $rcp_load_css, $rcp_load_scripts;

			if ( ( is_admin() && rcp_is_rcp_admin_page() ) || ( $rcp_load_css || $rcp_load_scripts ) ) {

				$dependencies = is_admin() ? array( 'rcp-admin-scripts' ) : array( 'rcp-register' );

				wp_enqueue_script(
					'rcp_sog_feed',
					"{$this->url}assets/js/script.js",
					$dependencies,
					self::VERSION,
					true
				);

				$select_choices = json_encode( $this->get_country_region_choices() );

				$script = <<<SCRIPT
rcp_sog_feed_select_choices = $select_choices;
SCRIPT;

				wp_add_inline_script(
					'rcp_sog_feed',
					$script,
					'after'
				);
			}
		}

		/**
		 * Callback for the `rcp_form_processing` action.
		 * Create Connections Entry based on submitted data during RCP member registration.
		 *
		 * @internal
		 * @since 2.0
		 *
		 * @param array $posted  Copy of the `$_POST` global.
		 * @param int   $user_id Current WP User ID.
		 */
		public function create_entry( $posted, $user_id ) {

			$user = get_user_by( 'id', $user_id );

			if ( function_exists( 'Connections_Directory' ) && ! self::is_user_linked( $user ) ) {

				//error_log( var_export( $posted, true ) );

				/*
				 * Register these actions because the `rcp_form_processing` action fires on `init` whereas
				 * these actions are not registered until `admin_init`.
				 */
				// Process entry categories.
				//add_action( 'cn_process_taxonomy-category', array( 'cnAdminActions', 'processEntryCategory' ), 9, 2 );

				// Entry Meta Action
				add_action( 'cn_process_meta-entry', array( 'cnAdminActions', 'processEntryMeta' ), 9, 2 );

				$data = array(
					'entry_type'   => 'individual',
					'first_name'   => $posted['rcp_user_first'],
					'last_name'    => $posted['rcp_user_last'],
					'organization' => isset( $posted['_sog_busname'] ) && ! empty( $posted['_sog_busname'] ) ? $posted['_sog_busname'] : '',
					'phone'        => array(
						array(
							'type'   => 'cellphone',
							'number' => isset( $posted['_sog_phone1'] ) && ! empty( $posted['_sog_phone1'] ) ? $posted['_sog_phone1'] : '',
						),
						array(
							'type'   => 'workphone',
							'number' => isset( $posted['_sog_phone2'] ) && ! empty( $posted['_sog_phone2'] ) ? $posted['_sog_phone2'] : '',
						),
					),
					'address'      => array(
						array(
							'type'    => 'work',
							'line_1'  => isset( $posted['_sog_address1'] ) && ! empty( $posted['_sog_address1'] ) ? $posted['_sog_address1'] : '',
							'line_2'  => isset( $posted['_sog_address2'] ) && ! empty( $posted['_sog_address2'] ) ? $posted['_sog_address2'] : '',
							'city'    => isset( $posted['_sog_city'] ) && ! empty( $posted['_sog_city'] ) ? $posted['_sog_city'] : '',
							'state'   => isset( $posted['_sog_state'] ) && ! empty( $posted['_sog_state'] ) ? $posted['_sog_state'] : '',
							'zipcode' => isset( $posted['_sog_zip'] ) && ! empty( $posted['_sog_zip'] ) ? $posted['_sog_zip'] : '',
							'country' => isset( $posted['_sog_country'] ) && ! empty( $posted['_sog_country'] ) ? $posted['_sog_country'] : '',
						),
					),
					'email'        => array(
						array(
							'type'    => 'work',
							'address' => $posted['rcp_user_email'],
						),
					),
					'visibility'   => 'public',
					'status'       => 'approved',
					'user'         => $user_id,
				);

				$entry_id = cnEntry_Action::add( $data );

				if ( false !== $entry_id && is_numeric( $entry_id ) ) {

					$regions     = $this->set_entry_region_term_relationships( $posted );
					$specialties = $this->set_entry_term_relationships( $posted );

					$termIDs = array_merge( $regions, $specialties );

					if ( 0 < count( $termIDs ) ) {

						// error_log( 'Settings Term Relationships: ' . json_encode( $termIDs ) );
						Connections_Directory()->term->setTermRelationships( $entry_id, $termIDs, 'category' );
					}

					/*
					 * Add the entry ID to the user meta to the user to that entry.
					 */
					update_user_meta( $user_id, 'connections_entry_id', $entry_id );
				}

			}

		}

		/**
		 * Callback for the `rcp_user_profile_updated` action.
		 *
		 * @internal
		 * @since 2.0
		 *
		 * @param int $user_id  Current WP User ID.
		 */
		public function update_entry( $user_id ) {

			$user = get_user_by( 'id', $user_id );

			if ( function_exists( 'Connections_Directory' ) && self::is_user_linked( $user ) ) {

				$posted  = $_POST;
				$entryID = self::get_linked_entry_id( $user );

				//error_log( var_export( $posted, true ) );
				//error_log( "Updating Entry ID: {$entryID}");

				if ( false === $entryID ) {

					return;
				}

				/*
				 * Register these actions because the `rcp_form_processing` action fires on `init` whereas
				 * these actions are not registered until `admin_init`.
				 */
				// Process entry categories.
				//add_action( 'cn_process_taxonomy-category', array( 'cnAdminActions', 'processEntryCategory' ), 9, 2 );

				// Entry Meta Action
				add_action( 'cn_process_meta-entry', array( 'cnAdminActions', 'processEntryMeta' ), 9, 2 );

				$data = array(
					//'entry_type'   => 'individual',
					'first_name'   => $posted['rcp_user_first'],
					'last_name'    => $posted['rcp_user_last'],
					'organization' => isset( $posted['_sog_busname'] ) && ! empty( $posted['_sog_busname'] ) ? $posted['_sog_busname'] : '',
					'phone'        => array(
						array(
							'type'   => 'cellphone',
							'number' => isset( $posted['_sog_phone1'] ) && ! empty( $posted['_sog_phone1'] ) ? $posted['_sog_phone1'] : '',
						),
						array(
							'type'   => 'workphone',
							'number' => isset( $posted['_sog_phone2'] ) && ! empty( $posted['_sog_phone2'] ) ? $posted['_sog_phone2'] : '',
						),
					),
					'address'      => array(
						array(
							'type'    => 'work',
							'line_1'  => isset( $posted['_sog_address1'] ) && ! empty( $posted['_sog_address1'] ) ? $posted['_sog_address1'] : '',
							'line_2'  => isset( $posted['_sog_address2'] ) && ! empty( $posted['_sog_address2'] ) ? $posted['_sog_address2'] : '',
							'city'    => isset( $posted['_sog_city'] ) && ! empty( $posted['_sog_city'] ) ? $posted['_sog_city'] : '',
							'state'   => isset( $posted['_sog_state'] ) && ! empty( $posted['_sog_state'] ) ? $posted['_sog_state'] : '',
							'zipcode' => isset( $posted['_sog_zip'] ) && ! empty( $posted['_sog_zip'] ) ? $posted['_sog_zip'] : '',
							'country' => isset( $posted['_sog_country'] ) && ! empty( $posted['_sog_country'] ) ? $posted['_sog_country'] : '',
						),
					),
					'email'        => array(
						array(
							'type'    => 'work',
							'address' => $posted['rcp_user_email'],
						),
					),
					'visibility'   => 'public',
					//'status'       => 'pending',
					//'user'         => $user_id,
				);

				$entry_id = cnEntry_Action::update( $entryID, $data );

				if ( false !== $entry_id && is_numeric( $entry_id ) ) {

					$regions     = $this->set_entry_region_term_relationships( $posted );
					$specialties = $this->set_entry_term_relationships( $posted );

					$termIDs = array_merge( $regions, $specialties );

					if ( 0 < count( $termIDs ) ) {

						// error_log( 'Settings Term Relationships: ' . json_encode( $termIDs ) );
						Connections_Directory()->term->setTermRelationships( $entry_id, $termIDs, 'category' );
					}
				}
			}
		}

		/**
		 * Set Connections Entry region term relationships.
		 *
		 * @since 2.0
		 *
		 * @param array $posted Copy of the `$_POST` global.
		 *
		 * @return int[]
		 */
		private function set_entry_region_term_relationships( $posted ) {

			$termIDs = array();

			if ( isset( $posted['_sog_country'] ) && !empty( $posted['_sog_country'] ) ) {
				$country = $posted['_sog_country'];
			}

			if ( isset( $posted['_sog_state'] ) && ! empty( $posted['_sog_state'] ) ) {
				$region = $posted['_sog_state'];
			}

			if ( ! isset( $country ) || ! isset( $region ) ) {

				return $termIDs;
			}

			$is_international = $country !== 'United States';

			$countryTerm = cnTerm::getBy( 'name', $country, 'category' );

			if ( $countryTerm instanceof cnTerm_Object ) {

				$regionTerm = cnTerm::getBy( 'name', $region, 'category' );

				if ( $regionTerm instanceof cnTerm_Object ) {

					$termIDs[] = $regionTerm->term_id;

				} else {

					$termArgs   = $is_international ? array( 'parent' => $countryTerm->term_id ) : array( 'parent' => 35 );
					$regionTerm = cnTerm::insert( $region, 'category', $termArgs );

					if ( ! $regionTerm instanceof WP_Error ) {

						$termIDs[] = $regionTerm['term_id'];
					}
				}

			} else {

				$termArgs    = $is_international ? array( 'parent' => 86 ) : array( 'parent' => 34 );
				$countryTerm = cnTerm::insert( $country, 'category', $termArgs );

				if ( ! $countryTerm instanceof WP_Error ) {

					$regionTerm = cnTerm::getBy( 'name', $region, 'category' );

					if ( $regionTerm instanceof cnTerm_Object ) {

						$termIDs[] = $regionTerm->term_id;

					} else {

						$termArgs   = $is_international ? array( 'parent' => $countryTerm['term_id'] ) : array( 'parent' => 35 );
						$regionTerm = cnTerm::insert( $region, 'category', $termArgs );

						if ( ! $regionTerm instanceof WP_Error ) {

							$termIDs[] = $regionTerm['term_id'];
						}
					}

				}
			}

			return $termIDs;
		}

		/**
		 * Set Connections Entry term relationships.
		 *
		 * @since 2.0
		 *
		 * @param array $posted Copy of the `$_POST` global.
		 *
		 * @return int[]
		 */
		private function set_entry_term_relationships( $posted ) {

			$termIDs = array();

			if ( isset( $posted['_sog_interests'] ) && is_array( $posted['_sog_interests'] ) ) {

				foreach ( $posted['_sog_interests'] as $interest ) {

					// error_log( "Searching for Term: {$interest}");
					$term = cnTerm::getBy( 'name', $interest, 'category' );
					// error_log( var_export( $term, true ) );

					if ( $term instanceof cnTerm_Object ) {

						// error_log( "Found Term: {$term->term_id}");
						$termIDs[] = $term->term_id;

					} else {

						// error_log( "Interest NOT found: {$interest}");
						$result = cnTerm::insert( $interest, 'category' );
						// error_log( var_export( $result, true ) );

						if ( ! $result instanceof WP_Error ) {

							// error_log( "New Term: {$result['term_id']}");
							$termIDs[] = $result['term_id'];
						}
					}
				}
			}

			return $termIDs;
		}

		/**
		 * @since 2.0
		 *
		 * @param WP_User $user
		 *
		 * @return bool
		 */
		final public static function is_user_linked( $user ) {

			global $wpdb;

			$userID = $wpdb->get_var(
				$wpdb->prepare(
					'SELECT `id` FROM ' . CN_ENTRY_TABLE . '  WHERE `user` = %d',
					$user->ID
				)
			);

			$entryID = get_user_meta( $user->ID, 'connections_entry_id', true );

			return ( ! is_null( $userID ) ) && ( ! empty( $entryID ) );
		}

		/**
		 * @param WP_User $user
		 *
		 * @return bool|int|string
		 */
		final public static function get_linked_entry_id( $user ) {

			$entryID = get_user_meta( $user->ID, 'connections_entry_id', true );

			return is_numeric( $entryID ) && ! empty( $entryID ) ? $entryID : false;
		}
	}

	/**
	 * The main function responsible for returning the instance to functions everywhere.
	 *
	 * Use this function like you would a global variable, except without needing to declare the global.
	 *
	 * Example: <?php $connector = Restrict_Content_Pro_SOG_Feed(); ?>
	 *
	 * If the main Connections class exists, fire up the connector. If not, throw an admin error notice.
	 *
	 * @access public
	 * @since  2.0
	 *
	 * @return Restrict_Content_Pro_SOG_Feed|false Restrict_Content_Pro_SOG_Feed Instance or FALSE if Connections is not active.
	 */
	function Connections_RCP_SOG_Feed() {

		if ( class_exists( 'connectionsLoad' ) ) {

			return Restrict_Content_Pro_SOG_Feed::instance();

		} else {

			return false;
		}

	}

	/**
	 * We'll load the extension on `plugins_loaded` so we know Connections will be loaded and ready first.
	 * Set priority 12, so we know Link is loaded first.
	 */
	add_action( 'plugins_loaded', __NAMESPACE__ . '\Connections_RCP_SOG_Feed', 12 );
}
