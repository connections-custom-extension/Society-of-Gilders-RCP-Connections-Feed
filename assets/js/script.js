var country_select = document.getElementById( '_sog_country' );
country_select.addEventListener( 'change', setRegionSelectOptions );

function setRegionSelectOptions() {
    var country_select = document.getElementById( '_sog_country' );

    var country_name = country_select.options[country_select.selectedIndex].value;

    var result = Object.keys( rcp_sog_feed_select_choices ).filter( function( index ) {
        return rcp_sog_feed_select_choices[index].name === country_name;
    } );

    var regions = rcp_sog_feed_select_choices[result].regions;

    var region_select = document.getElementById( '_sog_state' );

    // Remove all select options.
    var index = region_select.options.length;
    while ( index-- ) {
        region_select.remove( index );
    }

    // Add region option to the select element.
    for ( var key in regions ) {
        if ( regions.hasOwnProperty( key ) ) {

            var option = document.createElement( 'option' );

            option.text  = regions[key];
            option.value = regions[key];

            region_select.add( option, null );
        }
    }
}
