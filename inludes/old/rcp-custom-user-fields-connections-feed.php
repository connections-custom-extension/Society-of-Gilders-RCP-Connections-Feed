<?php
/*
Plugin Name: Restrict Content Pro - Connections Business Directory Feed
Description: Creates a Connections Entry on Restrict Content Pro registration and updates Connections Entry on changes made on the Restrict Content Pro Profile page.
Version: 1.0
Author: Steven A. Zahm
Author URI: https://connections-pro.com
*/

add_action(
	'rcp_form_processing',
	/**
	 * Create Connections Entry based on submitted data during RCP member registration.
	 *
	 * @param array $posted
	 * @param int   $user_id
	 */
	function( $posted, $user_id ) {

		$user = get_user_by( 'id', $user_id );

		if ( function_exists( 'Connections_Directory' ) && ! rcp_connections_feed_is_user_linked( $user ) ) {

			//error_log( var_export( $posted, true ) );

			/*
			 * Register these actions because the `rcp_form_processing` action fires on `init` whereas
			 * these actions are not registered until `admin_init`.
			 */
			// Process entry categories.
			//add_action( 'cn_process_taxonomy-category', array( 'cnAdminActions', 'processEntryCategory' ), 9, 2 );

			// Entry Meta Action
			add_action( 'cn_process_meta-entry', array( 'cnAdminActions', 'processEntryMeta' ), 9, 2 );

			$data = array(
				'entry_type'   => 'individual',
				'first_name'   => $posted['rcp_user_first'],
				'last_name'    => $posted['rcp_user_last'],
				'organization' => isset( $posted['_sog_busname'] ) && ! empty( $posted['_sog_busname'] ) ? $posted['_sog_busname'] : '',
				'phone'        => array(
					array(
						'type'   => 'cellphone',
						'number' => isset( $posted['_sog_phone1'] ) && ! empty( $posted['_sog_phone1'] ) ? $posted['_sog_phone1'] : '',
					),
					array(
						'type'   => 'workphone',
						'number' => isset( $posted['_sog_phone2'] ) && ! empty( $posted['_sog_phone2'] ) ? $posted['_sog_phone2'] : '',
					),
				),
				'address'      => array(
					array(
						'type'    => 'work',
						'line_1'  => isset( $posted['_sog_address1'] ) && ! empty( $posted['_sog_address1'] ) ? $posted['_sog_address1'] : '',
						'line_2'  => isset( $posted['_sog_address2'] ) && ! empty( $posted['_sog_address2'] ) ? $posted['_sog_address2'] : '',
						'city'    => isset( $posted['_sog_city'] ) && ! empty( $posted['_sog_city'] ) ? $posted['_sog_city'] : '',
						'state'   => isset( $posted['_sog_state'] ) && ! empty( $posted['_sog_state'] ) ? $posted['_sog_state'] : '',
						'zipcode' => isset( $posted['_sog_zip'] ) && ! empty( $posted['_sog_zip'] ) ? $posted['_sog_zip'] : '',
						'country' => isset( $posted['_sog_country'] ) && ! empty( $posted['_sog_country'] ) ? $posted['_sog_country'] : '',
					),
				),
				'email'        => array(
					array(
						'type'    => 'work',
						'address' => $posted['rcp_user_email'],
					),
				),
				'visibility'   => 'public',
				'status'       => 'approved',
				'user'         => $user_id,
			);

			$entry_id = cnEntry_Action::add( $data );

			if ( false !== $entry_id && is_numeric( $entry_id ) ) {

				if ( isset( $posted['_sog_interests'] ) && is_array( $posted['_sog_interests'] ) ) {

					$termIDs = array();

					foreach ( $posted['_sog_interests'] as $interest ) {

						//error_log( "Searching for Term: {$interest}");
						$term = cnTerm::getBy( 'name', $interest, 'category' );
						//error_log( var_export( $term, true ) );

						if ( $term instanceof cnTerm_Object ) {

							//error_log( "Found Term: {$term->term_id}");
							array_push( $termIDs, $term->term_id );

						} else {

							//error_log( "Interest NOT found: {$interest}");
							$result = cnTerm::insert( $interest, 'category' );
							//error_log( var_export( $result, true ) );

							if ( ! $result instanceof WP_Error ) {

								//error_log( "New Term: {$result['term_id']}");
								array_push( $termIDs, $result['term_id'] );
							}
						}
					}

					if ( 0 < count( $termIDs ) ) {

						//error_log( 'Settings Term Relationships: ' . json_encode( $termIDs ) );
						Connections_Directory()->term->setTermRelationships( $entry_id, $termIDs, 'category' );
					}

				}

				/*
				 * Add the entry ID to the user meta to the user to that entry.
				 */
				update_user_meta( $user_id, 'connections_entry_id', $entry_id );
			}

		}

	},
	11, // Set priority 11 because client plugin Restrict Content Pro - Custom Modifications has action hooked on priority 10 which must run first.
	2
);

add_action(
	'rcp_user_profile_updated',
	/**
	 * @param int     $user_id
	 * @param array   $userdata
	 * @param WP_User $old_data
	 */
	function( $user_id, $userdata, $old_data ) {

		$user = get_user_by( 'id', $user_id );

		if ( function_exists( 'Connections_Directory' ) && rcp_connections_feed_is_user_linked( $user ) ) {

			$posted  = $_POST;
			$entryID = rcp_connections_feed_get_linked_entry_id( $user );

			//error_log( var_export( $posted, true ) );
			//error_log( "Updating Entry ID: {$entryID}");

			if ( false === $entryID ) {

				return;
			}

			/*
			 * Register these actions because the `rcp_form_processing` action fires on `init` whereas
			 * these actions are not registered until `admin_init`.
			 */
			// Process entry categories.
			//add_action( 'cn_process_taxonomy-category', array( 'cnAdminActions', 'processEntryCategory' ), 9, 2 );

			// Entry Meta Action
			add_action( 'cn_process_meta-entry', array( 'cnAdminActions', 'processEntryMeta' ), 9, 2 );

			$data = array(
				//'entry_type'   => 'individual',
				'first_name'   => $posted['rcp_user_first'],
				'last_name'    => $posted['rcp_user_last'],
				'organization' => isset( $posted['_sog_busname'] ) && ! empty( $posted['_sog_busname'] ) ? $posted['_sog_busname'] : '',
				'phone'        => array(
					array(
						'type'   => 'cellphone',
						'number' => isset( $posted['_sog_phone1'] ) && ! empty( $posted['_sog_phone1'] ) ? $posted['_sog_phone1'] : '',
					),
					array(
						'type'   => 'workphone',
						'number' => isset( $posted['_sog_phone2'] ) && ! empty( $posted['_sog_phone2'] ) ? $posted['_sog_phone2'] : '',
					),
				),
				'address'      => array(
					array(
						'type'    => 'work',
						'line_1'  => isset( $posted['_sog_address1'] ) && ! empty( $posted['_sog_address1'] ) ? $posted['_sog_address1'] : '',
						'line_2'  => isset( $posted['_sog_address2'] ) && ! empty( $posted['_sog_address2'] ) ? $posted['_sog_address2'] : '',
						'city'    => isset( $posted['_sog_city'] ) && ! empty( $posted['_sog_city'] ) ? $posted['_sog_city'] : '',
						'state'   => isset( $posted['_sog_state'] ) && ! empty( $posted['_sog_state'] ) ? $posted['_sog_state'] : '',
						'zipcode' => isset( $posted['_sog_zip'] ) && ! empty( $posted['_sog_zip'] ) ? $posted['_sog_zip'] : '',
						'country' => isset( $posted['_sog_country'] ) && ! empty( $posted['_sog_country'] ) ? $posted['_sog_country'] : '',
					),
				),
				'email'        => array(
					array(
						'type'    => 'work',
						'address' => $posted['rcp_user_email'],
					),
				),
				'visibility'   => 'public',
				//'status'       => 'pending',
				//'user'         => $user_id,
			);

			$entry_id = cnEntry_Action::update( $entryID, $data );

			if ( false !== $entry_id && is_numeric( $entry_id ) ) {

				if ( isset( $posted['_sog_interests'] ) && is_array( $posted['_sog_interests'] ) ) {

					$termIDs = array();

					foreach ( $posted['_sog_interests'] as $interest ) {

						//error_log( "Searching for Term: {$interest}");
						$term = cnTerm::getBy( 'name', $interest, 'category' );
						//error_log( var_export( $term, true ) );

						if ( $term instanceof cnTerm_Object ) {

							//error_log( "Found Term: {$term->term_id}");
							array_push( $termIDs, $term->term_id );

						} else {

							//error_log( "Interest NOT found: {$interest}");
							$result = cnTerm::insert( $interest, 'category' );
							//error_log( var_export( $result, true ) );

							if ( ! $result instanceof WP_Error ) {

								//error_log( "New Term: {$result['term_id']}");
								array_push( $termIDs, $result['term_id'] );
							}
						}
					}

					if ( 0 < count( $termIDs ) ) {

						//error_log( 'Settings Term Relationships: ' . json_encode( $termIDs ) );
						Connections_Directory()->term->setTermRelationships( $entry_id, $termIDs, 'category' );
					}

				}

			}
		}
	},
	11, // Set priority 11 because client plugin Restrict Content Pro - Custom Modifications has action hooked on priority 10 which must run first.
	3
);

/**
 * @param WP_User $user
 *
 * @return bool
 */
function rcp_connections_feed_is_user_linked( $user ) {
	global $wpdb;

	$userID = $wpdb->get_var(
		$wpdb->prepare(
			'SELECT `id` FROM ' . CN_ENTRY_TABLE . '  WHERE `user` = %d',
			$user->ID
		)
	);

	$entryID = get_user_meta( $user->ID, 'connections_entry_id', true );

	return ( ! is_null( $userID ) ) && ( ! empty( $entryID ) );
}

/**
 * @param WP_User $user
 *
 * @return bool|int|string
 */
function rcp_connections_feed_get_linked_entry_id( $user ) {

	//global $wpdb;
	//
	//$userID = $wpdb->get_var(
	//	$wpdb->prepare(
	//		'SELECT `id` FROM ' . CN_ENTRY_TABLE . '  WHERE `user` = %d',
	//		$user->ID
	//	)
	//);

	$entryID = get_user_meta( $user->ID, 'connections_entry_id', true );

	//error_log( var_export( $entryID, true ) );

	return is_numeric( $entryID ) && ! empty( $entryID ) ? $entryID : false;
}

//add_action(
//	'rcp_update_payment_status_complete',
//	/**
//	 * @param int $payment_id
//	 */
//	function( $payment_id ) {
//
//		if ( ! function_exists( 'Connections_Directory' ) ) return;
//
//		$rcp_payments = new RCP_Payments();
//		$payment      = $rcp_payments->get_payment( $payment_id );
//
//		// The object ID is the subscription level ID.
//		// This only needs to run if the subscription if the "Individual" level.
//		if ( 3 == $payment->object_id ) {
//
//			$user_id  = $payment->user_id;
//			$entry_id = get_user_meta( $user_id, 'connections_entry_id', TRUE );
//
//			if ( ! empty( $entry_id ) ) {
//
//				//$data  = Connections_Directory()->retrieve->entry( $entry_id );
//				//$entry = new cnEntry( $data );
//				$entry = new cnEntry();
//				$entry->set( $entry_id );
//
//				$entry->setEntryType( 'individual' );
//				$entry->setFirstName( get_user_meta( $user_id, 'first_name', TRUE ) );
//				$entry->setLastName( get_user_meta( $user_id, 'last_name', TRUE ) );
//
//				$entry->update();
//			}
//		}
//
//	}
//);

add_action(
	'rcp_set_status',
	/**
	 * Set Entry Status based on Restrict Content Pro User Status
	 *
	 * @param string $new_status
	 * @param int    $user_id
	 * @param string $old_status
	 */
	function( $new_status, $user_id, $old_status ) {

		//error_log( var_export( rcp_get_customer_by_user_id( $user_id ), true ) );

		/** @var wpdb $wpdb */
		global $wpdb;

		// Get the entry ID of the user.
		$entry_id = $wpdb->get_var(
			$wpdb->prepare(
				'SELECT id FROM ' . CN_ENTRY_TABLE . ' WHERE user = %d',
				$user_id
			)
		);

		// Only continue to process if the user has a link entry.
		if ( ! empty( $entry_id ) ) {

			switch ( $new_status ) {

				case 'active':

					$entry = Connections_Directory()->retrieve->entry( $entry_id );

					$status     = 'approved';
					$visibility = 'expired' === $old_status ? 'public' : $entry->visibility;
					break;

				case 'inactive':
					$status     = 'approved';
					$visibility = 'unlisted';
					break;

				case 'cancelled':
					$status     = 'approved';
					$visibility = 'unlisted';
					break;

				default:
					$status     = 'approved';
					$visibility = 'unlisted';
			}

			$wpdb->update(
				CN_ENTRY_TABLE,
				array(
					'status'     => $status,
					'visibility' => $visibility,
					'edited_by'  => get_current_user_id(),
				),
				array( 'id' => $entry_id ),
				array(
					'%s',
					'%s',
					'%d'
				),
				array( '%d' )
			);

			rcp_add_member_note(
				$user_id,
				sprintf(
					'Connections Log Note: [RCP Old Status: %s] - [RCP  New Status: %s] - [User ID: %d] - [Entry ID: %d] - [Visibility: %s] - [Status: %s]',
					0 < strlen( $old_status ) ? $old_status : 'NONE',
					$new_status,
					$user_id,
					$entry_id,
					$visibility,
					$status
				)
			);

		} else {

			rcp_add_member_note(
				$user_id,
				sprintf(
					'Connections Log Note: [RCP Old Status: %s] - [RCP  New Status: %s] - [User ID: %d] - [Entry ID: NONE]',
					0 < strlen( $old_status ) ? $old_status : 'NONE',
					$new_status,
					$user_id
				)
			);
		}
	},
	10,
	3
);
